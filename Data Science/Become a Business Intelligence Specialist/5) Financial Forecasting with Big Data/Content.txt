FINANCIAL FORECAST WITH BIG DATA

Big data is transforming the world of business. Yet many people don't understand what big data and business intelligence 
are, or how to apply the techniques to their day-to-day jobs. This course addresses that knowledge gap, 
giving businesspeople practical methods to create quick and relevant business forecasts using big data.

Join Professor Michael McDonald and discover how to use predictive analytics to forecast key performance 
indicators of interest, such as quarterly sales, projected cash flow, or even optimized product pricing. 
All you need is Microsoft Excel. Michael uses the built-in formulas, functions, and calculations to perform 
regression analysis, calculate confidence intervals, and stress test your results. You'll walk away from the course 
able to immediately begin creating forecasts for your own business needs.

Objectives:
1. List the two methods of making decisions.
2. Identify the most common method of conventional financial forecasting.
3. Describe common challenges that come when trying to merge data.
4. Assess the types of questions that business intelligence is best suited to answer.
5. Distinguish the statistic that is most useful for estimating the impact of an X variable on a Y variable.

Content:
THE BASICS

1. What is big data?
2. Business intelligence and company financials
3. Basics of financial regression analysis
4. Predict values with regression analysis
5. Conventional financial forecasting
6. Questions

FORECASTING IN FINANCE

1. Decide on a finance question	
2. Gather financial data
3. Clean financial data
4. Questions

PERFORMING FORECASTING

1. Financial forecasting applications
2. Applied forecasting with data
3. Regressions for forecasting
4. Use Excel for regressions
5. Questions

INTERPRETING FORECAST RESULTS

1. What do the results mean?
2. Confidence intervals around the result
3. Perform stress testing
4. Questions

Certificate:
1. FMI - Certificate of Completion Financial Forecasting with Big Data
2. NASBA - Certificate of Completion Financial Forecasting with Big Data