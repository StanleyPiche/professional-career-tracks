Before anything, I would like to thank you for the interest you have had in learning more about my professional career.

The objective of this project is to show my professional areas of interest: Data Science and Finance, and what my level of education and experience related to them is, apart from my degree in Economics & Business.

At the top of this "README.me" section you will find this file along with two folders called "Data Science" and "Finance" where you can appreciate my preparation and experience in each of these areas, as well as the contents I have studied with their respective certification to support my knowledge.

In some folders you will see files of this type ".gitignore", just ignore them. I use these files to avoid uploading the content and complete development of each of the courses I have taken, as I must comply with the privacy policies of the different entities where I have been certified.

If you have any doubt, suggestion or comment you can contact me at "pichestan1999@gmail.com". It will be a pleasure to communicate with you.

Best regards!

Atte. Stanley Piche.